FROM php:8.1-apache

RUN apt update && apt install -y \
&& docker-php-ext-install mysqli pdo_mysql

RUN a2enmod rewrite

RUN chmod 777 -R /var/www
